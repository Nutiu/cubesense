/**************************************************************
 *
 * This sketch connects to a website and downloads a page.
 * It can be used to perform HTTP/RESTful API calls.
 *
 * TinyGSM Getting Started guide:
 *   http://tiny.cc/tiny-gsm-readme
 *
 **************************************************************/

// Select your modem:
#define TINY_GSM_MODEM_SIM800
#include "ThingspeakGprs.h"
unsigned long myChannelNumber = 218887;
const char * myWriteAPIKey = "CBLZNHOYC0FN4D8V";


// Your GPRS credentials
// Leave empty, if missing user or pass
const char apn[]  = "internet.eplus.de";
const char user[] = "blau";
const char pass[] = "blau";

// Use Hardware Serial on Mega, Leonardo, Micro
#define SerialAT Serial

// or Software Serial on Uno, Nano
//#include <SoftwareSerial.h>
//SoftwareSerial SerialAT(2, 3); // RX, TX

float sensorValues[8];

ThingspeakGprs tsgprs;

const char server[] = "api.thingspeak.com";
const char resource[] = "/update?api_key=CBLZNHOYC0FN4D8V&field1=";

void setup() {
  // Set console baud rate
  Serial.begin(115200);
  delay(10);
  // Set GSM module baud rate
  SerialAT.begin(115200);
  delay(3000);
  
  tsgprs.begin(apn, user, pass, myChannelNumber, myWriteAPIKey, SerialAT);
  // Restart takes quite some time
  // To skip it, call init() instead of restart()
  Serial.println("Initializing modem...");
  if (tsgprs.restartModem() < 0){
    Serial.print(tsgprs.getLastError());
    delay(10000);
  }; 
}
  
  int i = 0;
  
  void loop() {
    while (true){
      i = i+1;
      for (uint8_t j = 0; j< 8; j++){
      sensorValues[j] = j+i;   
      }
      
      tsgprs.sendDataToCloud(sensorValues);
      delay(120000);
      Serial.print("Next value ");
      Serial.println(i);
    }
    // Do nothing forevermore
}

