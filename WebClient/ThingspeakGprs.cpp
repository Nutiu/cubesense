/*
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#define THINGSPEAK_URL "api.thingspeak.com"
//const char resource[] = "/update?api_key=CBLZNHOYC0FN4D8V&field1=";
#include "ThingspeakGprs.h"
#define RESSOURCE_URI  "/update?api_key="  
    ThingspeakGprs::ThingspeakGprs(){};
    
    void ThingspeakGprs::begin(char* apn, char* user, char* pass, unsigned long myChannelNumber, char* chApiKey, Stream& serialInterface)
    {
       this->apn = apn;
       this->user = user;
       this->pass = pass;
       this->apiKey = chApiKey;          
       this->modem = new TinyGsm(serialInterface);
       this->client = new TinyGsmClient(*modem); 
       this->channel = myChannelNumber;
    }

    int ThingspeakGprs::simUnlock(char* pin){
     return  modem->simUnlock(pin);
    }
    int convertFloatToChar(float value, char *valueString)
    {
      // Supported range is -999999000000 to 999999000000
      if(0 == isinf(value) && (value > 999999000000 || value < -999999000000))
      {
        // Out of range
        return ERR_OUT_OF_RANGE;
      }
      // assume that 5 places right of decimal should be sufficient for most applications
  
          #if defined(ARDUINO_ARCH_SAMD) || defined(ARDUINO_ARCH_SAM)
        sprintf(valueString, "%.5f", value);
      #else
        dtostrf(value,1,5, valueString);
          #endif
      return OK_SUCCESS;
    };

    char* ThingspeakGprs::getLastError(){return lastError;};
    
    
    /*static int simUmlock(char* pin)  {
      return modem->simUnlock(pin);
    };*/
    
    int ThingspeakGprs::restartModem(){
        #ifdef PRINT_DEBUG_MESSAGES
        Serial.print("Waiting for network...");
        #endif
        if (!modem->begin()){
          lastError = "Modem initialisation error";
          return ERR_MODEM_INIT;
        }
        if (!modem->waitForNetwork()) {
          #ifdef PRINT_DEBUG_MESSAGES
          Serial.println(" fail");
          #endif
          //delay(1000);
          lastError = "Network registration failed";
          return ERR_NETWORK_REGISTRATION;
        }
        #ifdef PRINT_DEBUG_MESSAGES
        Serial.println(" OK");
        #endif
        return 0;
    };
    
    uint8_t ThingspeakGprs::sendDataToCloud(float values[8]){
    #ifdef PRINT_DEBUG_MESSAGES
      Serial.print("APN connect ");
      Serial.print(apn);
    #endif  
      if (!this->modem->gprsConnect(apn, user, pass)) {
    #ifdef PRINT_DEBUG_MESSAGES      
        Serial.println(" failed");
    #endif    
        delay(10000);
        return;
      }
    #ifdef PRINT_DEBUG_MESSAGES  
      Serial.println(" OK");
      Serial.print("HTTP connect to ");
      Serial.print(THINGSPEAK_URL);
    #endif  
      if (!client->connect(THINGSPEAK_URL, 80)) {
    #ifdef PRINT_DEBUG_MESSAGES      
        Serial.println(" failed");
    #endif              
        delay(10000);
        return;
      }
    #ifdef PRINT_DEBUG_MESSAGES      
        Serial.println(" OK");
    #endif          
      // Build request string
      
      // Make a HTTP GET request:
      this->client->print(String("GET ") + RESSOURCE_URI );
      this->client->print(apiKey);
      for (uint8_t i = 0; i< 8; i++){
        int status = convertFloatToChar(values[i], valueBuff);
        if (status == 0){
          this->client->print("&field" + String(i+1, DEC) + "=");
          this->client->print(valueBuff);
        }
      }
      this->client->print(" HTTP/1.0\r\n");
      this->client->print(String("Host: ") + THINGSPEAK_URL + "\r\n");
      this->client->print("Connection: close\r\n\r\n");

      unsigned long timeout = millis();
      while (this->client->connected() && millis() - timeout < 10000L) {
        // Print available data
        while (this->client->available()) {
          char c = this->client->read();
          #ifdef PRINT_DEBUG_MESSAGES                
          Serial.print(c);
          #endif
          timeout = millis();
        }
      }
      
      this->client->stop();
      #ifdef PRINT_DEBUG_MESSAGES                
      Serial.println("Server disconnected");
      #endif
      this->modem->gprsDisconnect();
      #ifdef PRINT_DEBUG_MESSAGES                
      Serial.println("GPRS disconnected");
      #endif
    };




