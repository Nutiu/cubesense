#ifndef ThingspeakGprs_h
#define ThingspeakGprs_h

#include <Arduino.h>

#define TINY_GSM_MODEM_SIM800
#include <TinyGsmClient.h>
#define ERR_OUT_OF_RANGE        -1  
#define OK_SUCCESS 0

#define ERR_NETWORK_REGISTRATION  -1
#define ERR_MODEM_INIT -2
#define PRINT_DEBUG_MESSAGES
class ThingspeakGprs {
  public:
    ThingspeakGprs();
  	void begin(char* apn, char* user, char* pass, unsigned long myChannelNumber, char* chApiKey, Stream& serialInterface);
    int restartModem();
  	uint8_t sendDataToCloud(float* values);
  	uint32_t read(void);
  	void end(void);
    int simUnlock(char* pin);
    char* getLastError();
  private:
    Client* client = NULL;
    TinyGsm* modem = NULL;
    char* apn  = NULL;
    char* user  = NULL;
    char* pass  = NULL;
    char* apiKey  = NULL;
    char* channel = NULL;
    char* lastError = "";
    char valueBuff[20];  
};

#endif
